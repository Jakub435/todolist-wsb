package pl.jakub.wsb.model;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

import pl.jakub.wsb.model.response.ToDoListResponse;
import pl.jakub.wsb.volleyAdapter.GsonRequest;

public class Position {
    private Long id;
    private Boolean isChecked;
    private String name;
    private Double amount;
    private Group group;

    public Position(String name, Double amount) {
        this.name = name;
        this.amount = amount;
        isChecked = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public Boolean toggle() {
        isChecked = !isChecked;
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
package pl.jakub.wsb.model;

public enum ApiUrl {
    GROUP("/api/group"),
    LIST("/api/list")
    ;
    private static final String BASE_URL = "http://192.168.1.7:8080";
    private final String url;

    ApiUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return BASE_URL + url;
    }
}

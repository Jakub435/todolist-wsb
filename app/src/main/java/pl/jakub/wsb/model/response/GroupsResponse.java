package pl.jakub.wsb.model.response;

import pl.jakub.wsb.model.Group;

import java.util.List;
import java.util.Set;

public class GroupsResponse {
    private List<Group> groups;

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}

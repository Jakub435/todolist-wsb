package pl.jakub.wsb.model.response;

public class IsCheckedResponse {
    private Boolean isChecked;

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}

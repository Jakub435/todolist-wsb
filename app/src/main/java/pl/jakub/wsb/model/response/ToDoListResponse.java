package pl.jakub.wsb.model.response;


import java.util.Set;

import pl.jakub.wsb.model.Position;

public class ToDoListResponse {
    private Set<Position> positions;

    public Set<Position> getPositions() {
        return positions;
    }

    public void setPositions(Set<Position> positions) {
        this.positions = positions;
    }
}

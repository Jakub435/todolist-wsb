package pl.jakub.wsb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import pl.jakub.wsb.adapter.GroupElementAdapter;
import pl.jakub.wsb.adapter.PositionAdapter;
import pl.jakub.wsb.model.ApiUrl;
import pl.jakub.wsb.model.Group;
import pl.jakub.wsb.model.Position;
import pl.jakub.wsb.model.response.GroupsResponse;
import pl.jakub.wsb.model.response.ToDoListResponse;
import pl.jakub.wsb.volleyAdapter.GsonRequest;

public class GroupDetails extends AppCompatActivity {

    private EditText elementName;
    private EditText elementAmount;
    private PositionAdapter adapter;
    private RequestQueue queue;
    private Long groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        queue = Volley.newRequestQueue(this);
        groupId = getIntent().getLongExtra("id", -1);

        queue.add(
                new GsonRequest<ToDoListResponse>(ApiUrl.LIST + "/" + groupId, Request.Method.GET, ToDoListResponse.class, null, this::initView, error -> {
                    throw new RuntimeException(error);
                })
        );
    }

    private void initView(ToDoListResponse response) {
        setContentView(R.layout.activity_group_details);

        ListView list = (ListView) findViewById(R.id.elementList);
        elementName = (EditText) findViewById(R.id.elementName);
        elementAmount = (EditText) findViewById(R.id.amount);
        Button returnButton = (Button) findViewById(R.id.returnButton);

        returnButton.setOnClickListener(v->{
            finish();
        });

        adapter = new PositionAdapter(new ArrayList<>(response.getPositions()), this, queue);

        list.setAdapter(adapter);

        findViewById(R.id.addNewElement).setOnClickListener(this::addElement);
    }


    public void addElement(View v) {
        queue.add(
                new GsonRequest<ToDoListResponse>(
                        ApiUrl.LIST + "/" + groupId,
                        Request.Method.POST,
                        ToDoListResponse.class,
                        new Position(
                                elementName.getText().toString(),
                                Double.valueOf(elementAmount.getText().toString())
                        ),
                        res->{
                            this.adapter.set(res.getPositions());
                        }, error -> { throw new RuntimeException(error); })
        );
    }
}
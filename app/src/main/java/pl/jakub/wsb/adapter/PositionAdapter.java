package pl.jakub.wsb.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import pl.jakub.wsb.R;
import pl.jakub.wsb.model.ApiUrl;
import pl.jakub.wsb.model.Position;
import pl.jakub.wsb.model.response.GroupsResponse;
import pl.jakub.wsb.model.response.IsCheckedResponse;
import pl.jakub.wsb.model.response.ToDoListResponse;
import pl.jakub.wsb.volleyAdapter.GsonRequest;

public class PositionAdapter extends BaseAdapter implements ListAdapter {
    private List<Position> list;
    private Context context;
    private RequestQueue queue;

    public PositionAdapter(List<Position> list, Context context, RequestQueue queue) {
        this.list = list;
        this.context = context;
        this.queue = queue;
    }

    public void add(Position element) {
        this.list.add(element);
    }

    public void set(Set<Position> element) {
        this.list = new ArrayList<>(element);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int i) {
        return this.list.get(i);
    }

    @Override
    public long getItemId(int i) {
        Position position = this.list.get(i);
        return position == null || position.getId() == null ? 0 : position.getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.single_element, null);
        }

        TextView elementName = (TextView) view.findViewById(R.id.singleElementName);
        TextView elementAmount = (TextView) view.findViewById(R.id.elementAmount);

        Position position = list.get(i);

        toggleElement(elementName, position.getChecked());
        toggleElement(elementAmount, position.getChecked());

        elementName.setText(position.getName());
        elementAmount.setText(position.getAmount().toString());

        Button removeButton = (Button)view.findViewById(R.id.removeElement);

        elementName.setOnClickListener(v->{
            Boolean isChecked = position.toggle();

            queue.add(
                    new GsonRequest<IsCheckedResponse>(ApiUrl.LIST + "/" + position.getId(), Request.Method.PUT, IsCheckedResponse.class, null, res->{
                        position.setChecked(isChecked);
                        notifyDataSetChanged();
                    }, error -> {
                        throw new RuntimeException(error);
                    })
            );
            toggleElement(elementName, isChecked);
            toggleElement(elementAmount, isChecked);
        });

        removeButton.setOnClickListener(v -> {

            queue.add(
                    new GsonRequest<Void>(ApiUrl.LIST + "/" + this.list.get(i).getId(), Request.Method.DELETE, Void.class, null, res->{
                        this.list.remove(i);
                        notifyDataSetChanged();
                    }, error -> {
                        throw new RuntimeException(error);
                    })
            );
        });

        return view;
    }

    private void toggleElement(TextView textView, Boolean isChecked) {
        if(isChecked) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
    }
}

package pl.jakub.wsb.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

import java.util.List;
import java.util.stream.Collectors;

import pl.jakub.wsb.GroupDetails;
import pl.jakub.wsb.R;
import pl.jakub.wsb.model.ApiUrl;
import pl.jakub.wsb.model.Group;
import pl.jakub.wsb.model.response.GroupsResponse;
import pl.jakub.wsb.volleyAdapter.GsonRequest;

public class GroupElementAdapter extends BaseAdapter implements ListAdapter {
    private List<Group> list;
    private Context context;
    private RequestQueue queue;

    public GroupElementAdapter(List<Group> list, Context context, RequestQueue queue) {
        this.list = list;
        this.context = context;
        this.queue = queue;
    }

    public void add(Group element) {
        this.list.add(element);
    }

    public void setGroups(List<Group> groups) {
        this.list = groups;
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int i) {
        return this.list.get(i);
    }

    @Override
    public long getItemId(int i) {
        Group group = this.list.get(i);
        return group == null ? 0 : group.getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.single_group, null);
        }

        TextView groupName = (TextView) view.findViewById(R.id.singleElementName);
        groupName.setText(list.get(i).getName());

        Button removeButton = (Button)view.findViewById(R.id.removeElement);

        groupName.setOnClickListener(v->{
            Intent intent = new Intent(context, GroupDetails.class);
            intent.putExtra("id", this.list.get(i).getId());

            context.startActivity(intent);
        });

        removeButton.setOnClickListener(v -> {
            removeGroup(this.list.get(i).getId());
        });

        return view;
    }

    private void removeGroup(Long id) {
        queue.add(
                new GsonRequest<Void>(ApiUrl.GROUP + "/" + id, Request.Method.DELETE, Void.class, null, res->{
                    this.list = this.list.stream().filter(g-> !g.getId().equals(id)).collect(Collectors.toList());
                    this.notifyDataSetChanged();
                }, error -> {
                    throw new RuntimeException(error);
                })
        );
    }
}

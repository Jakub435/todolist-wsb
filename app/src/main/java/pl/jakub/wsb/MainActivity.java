package pl.jakub.wsb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import pl.jakub.wsb.adapter.GroupElementAdapter;
import pl.jakub.wsb.model.ApiUrl;
import pl.jakub.wsb.model.Group;
import pl.jakub.wsb.model.response.GroupsResponse;
import pl.jakub.wsb.volleyAdapter.GsonRequest;

public class MainActivity extends AppCompatActivity {

    private EditText groupName;
    private GroupElementAdapter adapter;
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        queue = Volley.newRequestQueue(this);

        queue.add(
                new GsonRequest<GroupsResponse>(ApiUrl.GROUP.toString(), Request.Method.GET, GroupsResponse.class, null, this::initView, error -> {
                    throw new RuntimeException(error);
                })
        );
    }

    private void initView(GroupsResponse response) {
        setContentView(R.layout.activity_main);

        ListView list = (ListView) findViewById(R.id.groupList);
        groupName = (EditText) findViewById(R.id.groupName);

        adapter = new GroupElementAdapter(response.getGroups(), this, queue);

        list.setAdapter(adapter);
        findViewById(R.id.addNewGroup).setOnClickListener(this::addGroup);
    }

    public void addGroup(View v) {
        Group group = new Group(groupName.getText().toString());
        queue.add(
                new GsonRequest<GroupsResponse>(ApiUrl.GROUP.toString(), Request.Method.POST, GroupsResponse.class, group, res->{
                    this.adapter.setGroups(res.getGroups());
                    this.adapter.notifyDataSetChanged();
                }, error -> {
                })
        );
    }
}